-- 1. create a schema
create schema assignmentOne;
show schemas;
use assignmentone;

-- 2. Create table with fields - Id (number), First name (20 characters), Last name (20 characters), email (50 characters)
create table datas(ID int auto_increment primary key, first_name varchar(20), last_name varchar(20), email varchar(30) not null);

-- 3. add data to the table
insert into datas values(1, "Pasupathi", "Ganesan", "pasupathiganesan@gmail.com");
insert into datas(first_name, last_name, email) values("Sathish", "Kumar", "sathish@gmail.com"),
("Guna", "sekaran", "guna@gmail.com"), ("Giri", "tharan", "giritharan@gmail.com");

-- 4. Add a new columns - height (varchar - 3 characters), weight (number), extracol (boolean)
alter table datas add column height varchar(5), add column weight int, add column dummy boolean;

-- 5. Modify height column datatype to int
alter table datas modify column height int;
desc datas;

-- 6. add data to the newly added columns
update datas set height=150 where ID=1;
update datas set height=140 where ID>1 and ID<=3;
update datas set height=125 where ID=4;

update datas set height=130, weight=55 where ID=1;
update datas set height=135, weight=78 where ID>1 and ID<=3;
update datas set height=125, weight=90 where ID=4;

-- 7. drop extracol column
alter table datas drop column dummy;

-- 8. create 15 records into the table
insert into datas(first_name, last_name, email, height, weight) values("ram", "Kumar", "ram@gmail.com", 160, 67),
("siva", "sekaran", "siva@gmail.com", 150, 89), ("vasu", "tharan", "vasu@gmail.com", 165, 56),
("guru", "guru", "ram@gmail.com", 179, 78),("lord", "guna", "siva@gmail.com", 154, 52), 
("theeran", "semalai", "vasu@gmail.com", 152, 66),("kiruba", "karan", "kiruba@gmail.com", 145, 59),
("praba", "karan", "ram@gmail.com", 150, 98),("subash", "kumar", "siva@gmail.com", 166, 88), 
("nithee", "vedhalam", "vasu@gmail.com", 177, 66),("nithee", "vedhalam", "vasu@gmail.com", 140, 44);

-- 9. delete 7 records in the table where id range from 7 till 11
delete from datas where id between 16 and 22;
alter table datas add column dummy varchar(20);

-- 10. swap the first name and last name column
update datas set dummy = last_name;
update datas set last_name=first_name, first_name=dummy;

-- 11. Create new table with same structure and copy all the data into new table
create table datas1(select * from assignmentone.datas);

-- drop the extra column
alter table datas1 drop column dummy;

-- 12. truncate the old table
truncate datas;

-- 13. remove the old table
drop table datas;

-- select query
select * from assignmentone.datas;
select * from datas1;