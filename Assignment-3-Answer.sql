-- create the schema 
create schema assignmentthree;
show schemas;
use assignmentthree;

-- 1. "Denormalized" table into "Normalized table."
create table college_table(id int auto_increment primary key, name varchar(50), email varchar(30), started_year int, chairman_name varchar(20));
create table basic_details_table(id int auto_increment primary key, name varchar(20),age int, email varchar(30), aadhar bigint unique, pan varchar(20) unique, profession varchar(20));
create table mobile_table(id int auto_increment primary key, number bigint unique, plan varchar(20), operator varchar(20), validity varchar(20)); 
create table bus_table(id int auto_increment primary key, number varchar(10), reg_number varcharacter(20), brand varchar(20), model bigint, capacity int);

-- 2. Insert more than 20 records
insert into basic_details_table(name, age, email, aadhar, pan, profession)
values("Pasupathi", 25,  "pasupathi@gmail.com", 693764907731 , "DDXPP5598F" , "student"),
("ramesh", 45,  "ramesh@gmail.com", 666664907123 , "DDXMM9998F" , "student"),
("sukumar", 30,  "sukumar@gmail.com", 669994907123 , "DGGMM9998F" , "student"),
("murugan", 22,  "murugan@gmail.com", 693764901234 , "DDXPP5598A" , "student"),
("kumar", 22,  "kumar@gmail.com", 666664905678 , "DDXMM9998B" , "student"),
("sumankumar", 21,  "sumankumar@gmail.com", 669994907890 , "DGGMM9998C" , "student"),
("gugan", 21,  "gugan@gmail.com", 693764904321 , "DDXPP5598D" , "student"),
("raj", 22,  "raj@gmail.com", 699994907123 , "DDXMM9998E" , "student"),
("manoj", 23,  "manoj@gmail.com", 669994909654 , "DGGMM9999G" , "student"),
("sakthi", 22,  "sakthi@gmail.com", 693769999731 , "DDXPP1111F" , "student"),
("dhamo", 23,  "dhamo@gmail.com", 666668888123 , "DDXMW44448F" , "staff"),
("moosa", 23,  "moosa@gmail.com", 669995555523 , "DGGMB55555F" , "staff"),
("nikesh", 21,  "nikesh@gmail.com", 669996666663 , "DGGGG666666F" , "staff"),
("ramkumar", 21,  "ramkumar@gmail.com", 66911111 , "11111111" , "staff"),
("vijay", 21,  "vijay@gmail.com", 692222224321 , "D222222298D" , "staff"),
("ajith", 22,  "ajith@gmail.com", 6999933333333 , "DD33333338E" , "staff"),
("rajini", 23,  "rajini@gmail.com", 6644444444454 , "DGG4444449G" , "staff"),
("anushka", 21,  "anushka@gmail.com", 600000000001 , "D000000008D" , "staff"),
("samantha", 22,  "samantha@gmail.com", 61111333 , "DD1234" , "staff"),
("trish", 23,  "trish@gmail.com", 6611223344454 , "DG88776655G" , "staff"),
("kamar", 22,  "kamar@gmail.com", 655555559731 , "D5555555F" , "driver"),
("str", 23,  "str@gmail.com", 6666666677788123 , "D777777774448F" , "driver"),
("simbu", 23,  "simbu@gmail.com", 677777777773 , "D8888888855F" , "driver"),
("aniruth", 21,  "aniruth@gmail.com", 2222222223 , "D22222222F" , "driver"),
("gowtham", 22,  "gowtham@gmail.com", 1122339731 , "112233F" , "driver"),
("assan", 23,  "assan@gmail.com", 445566677788123 , "D7777766558F" , "driver"),
("mariyan", 23,  "mariyan@gmail.com", 68899066773 , "D4455667755F" , "driver"),
("king", 21,  "king@gmail.com", 4455663322 , "D224455663322" , "driver"),
("aravindh", 35,  "aravindh@gmail.com", 0909089786 , "DD0909089786" , "cleaner"),
("nandha", 45,  "nandha@gmail.com", 1121325432123 , "D11213254321238F" , "cleaner");

-- add column into "basic_details_table"
alter table basic_details_table add column bus_id int;

-- add "foreign key" to the "basic_details_table"
alter table basic_details_table add constraint bus_ref_fkey foreign key (bus_id) references bus_table (id);

-- insert the values into "mobile_table"
insert into mobile_table(number, operator, plan, validity)
values(9876543210, "jio", "50MBPS", "2023-12-12"),(9876888210, "vodafone", "50MBPS", "2023-12-12"),
(9876999210, "idea", "100MBPS", "2023-11-12"),(9999888210, "jio", "100MBPS", "2024-12-12"),
(9876855510, "airtel", "50MBPS", "2023-11-12"),(9876888333, "airtel", "100MBPS", "2024-12-17"),
(9876882233, "vodafone", "50MBPS", "2023-09-19"),(9874565210, "jio", "100MBPS", "2025-12-12"),
(9778888210, "airtel", "100MBPS", "2024-01-28"),(9876884455, "airtel", "50MBPS", "2024-07-12"),
(6676888210, "vodafone", "50MBPS", "2023-12-12"),(9877766210, "idea", "100MBPS", "2023-11-12"),
(9999888777, "jio", "100MBPS", "2024-12-12"),(9888855510, "airtel", "50MBPS", "2023-11-12"),
(9876889999, "vodafone", "50MBPS", "2023-09-19"),(9874565555, "jio", "100MBPS", "2025-12-12"),
(9778884433, "airtel", "100MBPS", "2024-01-28"),(9899994455, "airtel", "50MBPS", "2024-07-12"),
(9776654455, "airtel", "50MBPS", "2024-07-12"),(9999999999, "vodafone", "50MBPS", "2023-12-12"),
(6666666666, "idea", "100MBPS", "2023-11-12"),(11111111111, "jio", "100MBPS", "2024-12-12"),
(22222222222222, "airtel", "50MBPS", "2023-11-12"),(876881111, "airtel", "100MBPS", "2024-12-17"),
(876889999, "vodafone", "50MBPS", "2023-09-19"),(874565555, "jio", "100MBPS", "2025-12-12"),
(778884433, "airtel", "100MBPS", "2024-01-28"),(899994455, "airtel", "50MBPS", "2024-07-12"),
(776654455, "airtel", "50MBPS", "2024-07-12"),(676888210, "vodafone", "50MBPS", "2023-12-12"),
(877766210, "idea", "100MBPS", "2023-11-12"),(999888777, "jio", "100MBPS", "2024-12-12"),
(888855510, "airtel", "50MBPS", "2023-11-12"),(33333333333333, "airtel", "100MBPS", "2024-12-17"),
(4444444444444, "vodafone", "50MBPS", "2023-09-19"),(5555555555, "jio", "100MBPS", "2025-12-12"),
(6666666666666, "airtel", "100MBPS", "2024-01-28"),(777777777777, "airtel", "50MBPS", "2024-07-12"),
(8888888888888, "airtel", "50MBPS", "2024-07-12"),(99999000009999, "airtel", "100MBPS", "2024-12-17");

-- add the column in "mobile_table"
alter table mobile_table add column ref_id int;

-- add "Foreign key" constraint
alter table mobile_table add constraint mob_fkey foreign key (ref_id) references basic_details_table (id);
desc mobile_table;

-- insert the values to "bus_table"
insert into bus_table(number, reg_number, brand, model, capacity)
values(1, "TN24AJ2656", "BAJAJ", 2019, 30),(2, "TN01AJ2656", "BAJAJ", 2019, 30),
(3, "TN25AJ2656", "BAJAJ", 2019, 30),(4, "TN38AJ2656", "ASHOKLEYLAND", 2021, 40),
(5, "TN43BA2656", "ASHOKLEYLAND", 2021, 40),(6, "TN31BA2656", "ASHOKLEYLAND", 2021, 30),
(7, "TN28BJ2656", "ASHOKLEYLAND", 2022, 30),(8, "TN24AJ2858", "BENZ", 2022, 35),
(9, "TN24BA5656", "BENZ", 2023, 40),(10, "TN24AJ0011", "BENZ", 2023, 30);

--  Get all the Mobile numbers of "staff,driver,student, cleaner"
select basic.id, basic.name, basic.profession, mobile.number from basic_details_table as basic
inner join mobile_table as mobile
on basic.id=mobile.ref_id;

-- 3. Get all the Mobile numbers of "students"
select basic.id, basic.name, basic.profession, mobile.number from basic_details_table as basic
inner join mobile_table as mobile
on basic.id = mobile.ref_id
where profession = "student";

--  Get all the Mobile numbers of "staff"
select basic.id, basic.name, basic.profession, mobile.number from basic_details_table as basic
inner join mobile_table as mobile
on basic.id=mobile.ref_id
where profession="staff";

-- Get all the Mobile numbers of "driver"
select basic.id, basic.name, basic.profession, mobile.number from basic_details_table as basic
inner join mobile_table as mobile
on basic.id=mobile.ref_id
where profession="driver";

-- 4. Get the list of students who are not travelling in college bus --> using "is Null" keyword
select * from basic_details_table where profession="student" and bus_id is null; 

-- doesn't need to join the table
select basic.id, basic.name, basic.profession, basic.bus_id from basic_details_table as basic 
inner join bus_table as bus 
on basic.id = bus.id where bus_id is null;

-- student has mobile number
select basic.id, basic.name, basic.profession, mobile.number from basic_details_table as basic
inner join mobile_table as mobile
on basic.id = mobile.ref_id where profession='student';

-- list of students who are travelling in college bus
select * from basic_details_table where profession="student" and bus_id is not null;

-- 5. Get the list of students who are travelling in college bus but not having the mobile number
select * from basic_details_table where profession="student" and bus_id is not null and 
id not in(select basic.id from basic_details_table as basic
inner join mobile_table as mobile
on basic.id = mobile.ref_id where profession='student');

-- 6. Get the number of students and staffs travelling in bus wise - doesn't need to join the table
select bus_id,profession, count(profession) as count from basic_details_table 
where profession in ("student", "staff") and bus_id is not null group by bus_id, profession order by bus_id;

-- 7. Get the bus which has least number of people travelling
select bus_id, count(profession) from basic_details_table 
where profession in ("student", "staff") and bus_id is not null group by bus_id order by bus_id asc;

-- join 2 tables --> without condition
select * from basic_details_table as basic
left join mobile_table as mobile
on basic.id = mobile.id;

-- join the 2 tables --> with particulat column
select basic.name, basic.profession, basic.aadhar, mobile.number  from basic_details_table as basic
inner join mobile_table as mobile
on basic.id = mobile.id;

-- join 3 tables --> basic_details_table, mobile_table, bus_table
select basic.name, basic.profession, basic.aadhar, mobile.number, bus.reg_number from basic_details_table as basic
inner join mobile_table as mobile on basic.id = mobile.id 
inner join bus_table as bus on basic.id = bus.id;

-- join the table for single table ("De-Normalization Table")
select * from basic_details_table basic, mobile_table mobile, bus_table bus
where basic.id=mobile.ref_id and basic.id=bus.id;

-- joining without using join
select basic.id, basic.name, basic.aadhar, mobile.number, bus.reg_number from basic_details_table basic, mobile_table mobile, bus_table bus
where basic.id=mobile.ref_id and basic.id=bus.id;

-- Read the table 
select * from college_table;
select * from basic_details_table;
select * from mobile_table;
select * from bus_table;