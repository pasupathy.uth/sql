-- drop the schema
drop schema assignmenttwo;

-- create the schema
create schema assignmenttwo;
show schemas;
use assignmenttwo;

-- 1. create a citizen table and fields are id, name, age, email, salary, aadhar, pan
-- 2. In this table (email, aadhar, pan) should be UNIQUE and id should be primary.
create table citizen(id int auto_increment primary key, name varchar(20), age int, email varchar(20) unique, salary bigint, aadhar bigint unique, pan varchar(20) unique); 

insert into citizen(name, age, email, salary, aadhar, pan) 
values ('Pasupathi', 26, "pasupathi@gamil.com", 58, 693764907731, "DDXPP5598F"),
('sathish', 2, "sathish@gamil.com", 0, 693764907732, "DDXPP5598A"),
('theeran', 1, "theeran@gamil.com", 0, 693764907733, "DDXPP5598B"),
('gunasekaran', 25, "gunasek@gamil.com", 50, 693764907734, "DDXPP5598C"),
('giritharan', 26, "giritharan@gamil.com", 60, 693764907735, "DDXPP5598D"),
('praveen kumar', 30, "pasu@gamil.com", 70, 693764907736, "DDXPP5598E"),
('praneesh', 10, "praneesh@gamil.com", 0, 693764907737, "DDXPP5598G"),
('pradeep kumar', 12, "pradeep@gamil.com", 0, 693764907738, "DDXPP5598H"),
('namasivayam', 60, "siva@gamil.com", 80, 693764907739, "DDXPP5598I"),
('santheep kumar', 17, "santheep@gamil.com", 10, 693764907740, "DDXPP5598J"),
('senthil', 22, "senthil@gamil.com", 5, 693764907741, "DDXPP5598K"),
('saravanan', 80, "saravanan@gamil.com", 30, 693764907742, "DDXPP5598L"),
('subham', 68, "subham@gamil.com", 78, 693764907743, "DDXPP5598M"),
('subbu', 45, "subbu@gamil.com", 76, 693764907744, "DDXPP5598N"),
('suman', 29, "suman@gamil.com", 59, 693764907745, "DDXPP5598O"),
('saran', 61, "saran@gamil.com", 70, 693764907746, "DDXPP5598P"),
('aswin', 26, "aswin@gamil.com", 62, 693764907747, "DDXPP5598Q"),
('dinesh', 90, "dinesh@gamil.com", 41, 693764907748, "DDXPP5598R"),
('padma', 26, "padma@gamil.com", 40, 693764907749, "DDXPP5598S"),
('ram', 67, "ram@gamil.com", 76, 693764907750, "DDXPP5598T"),
('kali', 15, "kali@gamil.com", 0, 693764907751, "DDXPP5598U")
;
/* 3. The resultset should have two new columns like 
		a. category
		b. Voting Eligibility: result should be major/minor. If age is >= 18 then Major else minor. */
select *,
case 
	when age<3 then 'Kids' 	
	when age>=3 and age<11 then 'Junior' 	
	when age>=11 and age<16 then 'Secondary' 	
	when age>=16 and age<18 then 'Higher Seconday' 	
	when age>=18 and age<24 then 'College Student' 	
	when age>=24 and age<58 then 'Employee' 	
	when age>=58 and age<70 then 'Senior Citizen' 	
	when age>=70 then 'super senior citizen'
end as age_category,
case
	when age>=18 then 'Major'
    else 'Minor'
end as Voting_Eligibility from citizen;

/*
4. Group the people based on age like: 
		20 to 30 - group-1 
		30 to 40 - group-2 
		40 to 50 - group-3 
		50 to 60 - group-4
*/
-- a. what is maximum salary of the group?
select avg(salary) as avg_sal,
case 
	when age between 20 and 30 then 'group-1' 
    when age between 31 and 40 then 'group-2'
    when age between 41 and 50 then 'group-3' 
    when age between 51 and 60 then 'group-4'
end as group_name from citizen group by age order by avg(salary) desc limit 1;

-- b. what is minimum salary of the group?
select avg(salary) as avg_sal,
case 
	when age between 20 and 30 then 'group-1' 
    when age between 31 and 40 then 'group-2'
    when age between 41 and 50 then 'group-3' 
    when age between 51 and 60 then 'group-4'
end as group_name from citizen group by age order by avg(salary) limit 1;

-- c. which group is highest salary?
select 
case 
	when age between 20 and 30 then 'group-1' 
    when age between 31 and 40 then 'group-2'
    when age between 41 and 50 then 'group-3' 
    when age between 51 and 60 then 'group-4'
end as group_name from citizen group by age order by avg(salary) desc limit 1;

-- d. ignore the group average salary is less than 50
select avg(salary) as avg_sal,
case 
	when age between 20 and 30 then 'group-1' 
    when age between 31 and 40 then 'group-2'
    when age between 41 and 50 then 'group-3' 
    when age between 51 and 60 then 'group-4'
end as group_name from citizen group by age having avg(salary)>50;

-- e. Name of the persons whose salary is max in each group
select id,name,salary,
case 
	when age between 20 and 30 then 'group-1' 
    when age between 31 and 40 then 'group-2'
    when age between 41 and 50 then 'group-3' 
    when age between 51 and 60 then 'group-4'
end as group_name from citizen where age between 20 and 60 and salary in (
select max(salary) from citizen where age between 20 and 60 
group by age between 20 and 30, age between 31 and 40, age between 41 and 50, age between 51 and 60);

-- 5. create a view and the category resultset should be shown to view (point 3)
create view eligibility_view as (select *,
case 
	when age<3 then 'Kids' 	
	when age>=3 and age<11 then 'Junior' 	
	when age>=11 and age<16 then 'Secondary' 	
	when age>=16 and age<18 then 'Higher Seconday' 	
	when age>=18 and age<24 then 'College Student' 	
	when age>=24 and age<58 then 'Employee' 	
	when age>=58 and age<70 then 'Senior Citizen' 	
	when age>=70 then 'super senior citizen'
end as age_category,
case
	when age>=18 then 'Major'
    else 'Minor'
end as Voting_Eligibility from citizen);

-- 6. create the trigger for citizen table (insert/update/delete)
CREATE TABLE `citizen_trigger` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `age` int DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `salary` bigint DEFAULT NULL,
  `aadhar` bigint DEFAULT NULL,
  `pan` varchar(20) DEFAULT NULL,
  `operation` varchar(20) default null,
  PRIMARY KEY (`id`)
);

-- create a trigger for "INSERT"
DELIMITER $$
create trigger insert_trigger after insert on citizen for each row
begin
	insert into citizen_trigger(name, age, email, salary, aadhar, pan, operation)
    values(new.name, new.age, new.email, new.salary, new.aadhar, new.pan, "Insert");
end$$
DELIMITER ;

insert into citizen(name, age, email, salary, aadhar, pan) values("dummy111", 50, "dum123@gmail.com", 15, 33331116666, "abbbbbbbaaa");

-- create a trigger for "UPDATE"
DELIMITER $$
create trigger update_trigger after update on citizen for each row
begin
	insert into citizen_trigger(name, age, email, salary, aadhar, pan, operation)
    values(old.name, old.age, old.email, old.salary, old.aadhar, old.pan, "Update");
end$$
DELIMITER ;
update citizen set name="DummyName",email="dummy123@gmail.com" where aadhar=33331116666 and pan="abbbbbbbaaa";

-- create a trigger for "DELETE"
DELIMITER $$
create trigger delete_trigger after delete on citizen for each row
begin 
	insert into citizen_trigger(name, age, email, salary, aadhar, pan, operation)
    values(old.name, old.age, old.email, old.salary, old.aadhar, old.pan, "Delete");
end$$
DELIMITER ;
delete from citizen where ID in (22,23);

-- 7. create the user-defined function for citizen table which will sum the salary for all citizens
DELIMITER $$
create function sumSal()
returns bigint deterministic
begin
	declare sal bigint;
    select sum(salary) into sal from assignmenttwo.citizen;
    return sal;
end$$
DELIMITER ;

select assignmenttwo.sumSal() as sum_of_salary;

-- 8. show all the citizen names which ends with "kumar"
select * from citizen where name like "%kumar";

-- to view the resultset
select * from citizen;
